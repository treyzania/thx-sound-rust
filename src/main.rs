/*
 * Roughly translated from Java.  Some edits make to make it more rustic.
 * Original: https://gist.github.com/JoBa97/c371bf526d36f337e6128214dc10d8c7
 */

#![allow(unused)]

extern crate rand;

use rand::prng;
use rand::{Rng, RngCore, SeedableRng};

const SAMPLE_RATE: u32 = 128 * 1024;

fn main() {
    println!("Hello, world!");
}

fn generate_thx(seed: [u8; 16]) -> Vec<u8> {

    let mut rng = prng::XorShiftRng::from_seed(seed);

    let mut target_accord = vec![
        1480, 1480, 1480,
        1175, 1175, 1175,
        880, 880, 880,
        587, 587, 587,
        440, 440, 440,
        294, 294, 294,
        220, 220,
        147, 147,
        110, 110,
        73, 73,
        55, 55
    ];

    let line_cnt = target_accord.len();

    // Wiggle the values up an down a bit.  This might not actually do anything, it's just here.
    for i in 0..line_cnt {
        let mistake: f64 = 0.01;
        let delta: f64 = rng.gen();
        target_accord[i] += (1.0 + (delta * mistake - (mistake / 2.0))) as i32;
    }

    let speed = 4000;
    let min = 200;
    let max = 400;
    let min_steps = 3;
    let max_steps = 10;
    let duration_random = speed * 3;
    let duration_walk = speed * 2;
    let duration_hold = speed * 1;
    let duration_fade = speed * 1;
    let max_chg = 50;

    let mut accord_rand: Vec<Vec<f64>> = vec![Vec::new(); line_cnt];
    let mut accord_walk: Vec<Vec<f64>> = vec![Vec::new(); line_cnt];
    let mut accord_hold: Vec<Vec<f64>> = vec![Vec::new(); line_cnt];
    let mut accord_fade: Vec<Vec<f64>> = vec![Vec::new(); line_cnt];

    let mut random_walks: Vec<Vec<i32>> = vec![Vec::new(); line_cnt];
    for i in 0..line_cnt {
        let steps: i32 = rng.gen_range(min_steps, max_steps);
        random_walks[i] = random_walk(steps, min, max, max_chg);
    }

    let mut accord_walks: Vec<[i32; 2]> = vec![[0, 0]; line_cnt];
    for i in 0..line_cnt {
        accord_walks[i] = [random_walks[i][random_walks[i].len() - 1], target_accord[i]];
    }

    let mut accord_holds: Vec<[i32; 2]> = vec![[0, 0]; line_cnt];
    for i in 0..line_cnt {
        accord_holds[i] = [target_accord[i], target_accord[i]];
    }

    // Triangle waves!
    let mut state = 0.0;
    let mut sign = 0.0;
    for i in 0..line_cnt {
        state = -1.0;
        sign = 1.0;

        // Yay for inconsistent array concepts..  This is Java's fault.
        accord_rand[i] = generate_tone(random_walks[i].as_slice(), duration_random);
        accord_walk[i] = generate_tone(&accord_walks[i], duration_walk);
        accord_hold[i] = generate_tone(&accord_holds[i], duration_hold);
        accord_fade[i] = generate_tone(&accord_holds[i], duration_fade);
    }

    // Merge things now.
    let mut accord_random_merged = merge(accord_rand);
    let mut accord_walk_merged = merge(accord_walk);
    let mut accord_hold_merged = merge(accord_hold);
    let mut accord_fade_merged = merge(accord_fade);

    // Filter noise.
    let filter_start = 2000f32;
    let filter_end = 6000f32;
    let walk_filter_percentage_start = 0.5f32;

    let mut filter = Filter::new(filter_start, SAMPLE_RATE, PassType::LowPass, 1f32);
    for i in 0..accord_random_merged.len() {
        filter.update(accord_random_merged[i]);
        accord_random_merged[i] = filter.get_value();
    }

    for i in 0..accord_walk_merged.len() {
        if i >= accord_walk_merged.len() * walk_filter_percentage_start as usize {
            let c = (i as f32 - (accord_walk_merged.len() as f32) * walk_filter_percentage_start) as i32;
            // float next = filterStart + (filterEnd - filterStart) * ((float) c / (float) (accordWalkMerged.length * walkFilterPercentageStart));
            let next = filter_start + (filter_end - filter_start) * (c as f32 / (accord_walk_merged.len() as f32 * walk_filter_percentage_start));
            filter.change_frequency(next);
        }
        filter.update(accord_walk_merged[i]);
        accord_walk_merged[i] = filter.get_value();
    }

    for i in 0..accord_hold_merged.len() {
        filter.update(accord_hold_merged[i]);
        accord_hold_merged[i] = filter.get_value();
    }

    for i in 0..accord_fade_merged.len() {
        filter.update(accord_fade_merged[i]);
        accord_fade_merged[i] = filter.get_value();
    }

    let mut rising = concat(vec![accord_random_merged, accord_walk_merged, accord_hold_merged]);

    // Scaling things.
    scale_f64(&mut accord_fade_merged, 0x7fff, 0);
    scale_f64(&mut rising, 1, 0x7fff);

    let d_tone = concat(vec![rising, accord_fade_merged]);

    // Actual output
    f64_to_u8_direct(d_tone)

}

fn random_walk(steps: i32, min: i32, max: i32, max_chg: i32) -> Vec<i32> {
    unimplemented!()
}

fn generate_tone(a: &[i32], duration: u32) -> Vec<f64> {
    unimplemented!()
}

fn merge(tones: Vec<Vec<f64>>) -> Vec<f64> {
    unimplemented!()
}

fn concat(tones: Vec<Vec<f64>>) -> Vec<f64> {
    unimplemented!()
}

fn scale_f64(v: &mut Vec<f64>, start: u32, end: u32) {
    for i in 0..v.len() {
        let vol = start as f64 + (end as f64 - start as f64) * (i as f64 / v.len() as f64);
        v[i] *= vol;
    }
}

fn f64_to_u8_direct(vec: Vec<f64>) -> Vec<u8> {
    let mut out = vec![0; vec.len() * 2];
    for i in 0..vec.len() {
        let masked = vec[i] as u32; // ???
        out[i * 2] = ((masked >> 8) & 0xff) as u8;
        out[i * 2 + 1] = (masked & 0xff) as u8;
    }
    out
}

enum PassType {
    HighPass,
    LowPass
}

struct Filter {
    resonance: f32,
    sample_rate: u32,
    pass_type: PassType,

    // And now for a bunch of random stuff that's entirely undocumented.
    value: f64,
    c: f64,
    a1: f64,
    a2: f64,
    a3: f64,
    b1: f64,
    b2: f64,
    input_hist: [f64; 2],
    output_hist: [f64; 3]
}

impl Filter {
    fn new(freq: f32, rate: u32, ty: PassType, res: f32) -> Filter {
        let mut tmp = Filter {
            resonance: res,
            sample_rate: rate,
            pass_type: ty,

            value: 0.0,
            c: 0.0,
            a1: 0.0,
            a2: 0.0,
            a3: 0.0,
            b1: 0.0,
            b2: 0.0,
            input_hist: [0.0, 0.0],
            output_hist: [0.0, 0.0, 0.0]
        };
        tmp.change_frequency(freq);
        tmp
    }
    fn change_frequency(&mut self, freq: f32) {
        // Need trig functions for this.
        match self.pass_type {
            PassType::LowPass => {
                unimplemented!()
            },
            PassType::HighPass => {
                unimplemented!()
            }
        }
    }
    fn update(&mut self, next: f64) {
        let next_out = self.a1 * next + self.a2 * self.input_hist[0] + self.a3 * self.input_hist[1] - self.b1 * self.output_hist[0] - self.b2 * self.output_hist[1];
        self.input_hist[1] = self.input_hist[0];
        self.input_hist[0] = next;
        self.output_hist[2] = self.output_hist[1];
        self.output_hist[1] = self.output_hist[0];
        self.output_hist[0] = next_out;
    }
    fn get_value(&self) -> f64 {
        self.output_hist[0]
    }
}
